package self.pcchack.androidmonitor.fragment;

import java.net.Socket;

import pl.pcchack.fragment.TitledFragment;
import pl.pcchack.layoutgenerator.Generator;
import pl.pcchack.layoutgenerator.Generator.BindedLayout;
import self.pcchack.androidmonitor.activity.R;
import self.pcchack.androidmonitor.model.PortScannerData;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PortScannerFragment extends TitledFragment implements View.OnClickListener {
	public static int INFO_ID=123;
	private PortScannerData data;
	private TextView infoView;
	private ScanTask task;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Generator generator = new Generator(getActivity());
		data = new PortScannerData();
		BindedLayout bindedLayout = generator.createFormWithButtonAndInfo(data,this,R.string.scan, INFO_ID );
		LinearLayout layout = bindedLayout.getLayout();
		infoView = (TextView)layout.findViewById(INFO_ID);
		return layout;
		
	}
	public PortScannerData getData() {
		return data;
	}
	@Override
	public void onClick(View v) {
		Activity activity = getActivity();
		activity.setProgressBarVisibility(true);
		activity.setProgressBarIndeterminate(false);
		task = new ScanTask();
		task.execute();

		
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if(task != null && task.getStatus() == Status.RUNNING)
			task.cancel(true);
	}
	private boolean scan(short port) {
		try {
			new Socket(data.getUrl(),port);
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	private class ScanTask extends AsyncTask<Short,Short,StringBuilder> {
		@Override
		protected StringBuilder doInBackground(Short... params) {
			StringBuilder results = new StringBuilder();
			for(short port = data.getStartPort(),counter=0,n=(short)(10000/(data.getEndPort()-data.getStartPort()));port<data.getEndPort()+1;port++,counter+= n) {
				results.append(
						getActivity().getResources()
								.getString(scan(port) ?
										R.string.port_opened :
										R.string.port_closed,port)+"\n");
				publishProgress(counter);
			}
			return results;
		}
		@Override
		protected void onPostExecute(StringBuilder results) {
			infoView.setText(results.toString());
		}
		@Override
		protected void onProgressUpdate(Short... values) {
			getActivity().setProgress(values[0]);
		}
		
	}
}
