package self.pcchack.androidmonitor.fragment;

import pl.pcchack.activity.ComponentUtil;
import pl.pcchack.activity.ListViewAdapter;
import pl.pcchack.db.AndroidDao;
import pl.pcchack.fragment.TitledFragment;
import self.pcchack.androidmonitor.activity.R;
import self.pcchack.androidmonitor.model.Log;
import self.pcchack.androidmonitor.service.PhoneActionsReceiver;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

public class LogFragment extends TitledFragment implements OnClickListener, DialogInterface.OnClickListener {
	private ListView listView;
	private ListViewAdapter<Log> adapter;
	private Button clearButton;
	private AndroidDao dao; 
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view =  inflater.inflate(R.layout.log_fragment, null,false);
		initComponents(view);
		return view;
	}
	private void initComponents(View view) {
		dao = AndroidDao.getLastInstanceOrCreate(getActivity(), 
				PhoneActionsReceiver.LOGS_DB, 
				Log.class);
		adapter =new ListViewAdapter<Log>(getActivity(),
				dao.getAll(Log.class,Log.P_OCCURENCE_TIME,false));
		
		listView =(ListView)view.findViewById(R.id.logs_list_view);
		listView.setAdapter(adapter);
		clearButton =(Button) view.findViewById(R.id.clear_button);
		clearButton.setOnClickListener(this);
	}
	@Override
	public void onClick(View v) {
		ComponentUtil.showConfirm(getActivity(),this,
				getActivity().getResources().getString(
				R.string.clear_logs_confirm));
	}
	@Override
	public void onClick(DialogInterface dialog, int which) {
		dao.deleteAll(Log.class);
		adapter.clear();
	}
	
}
