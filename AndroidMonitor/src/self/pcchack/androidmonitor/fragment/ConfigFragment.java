package self.pcchack.androidmonitor.fragment;

import pl.pcchack.activity.AbstractActivity;
import pl.pcchack.fragment.TitledFragment;
import pl.pcchack.layoutgenerator.Generator;
import self.pcchack.androidmonitor.activity.MonitorActivity;
import self.pcchack.androidmonitor.model.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ConfigFragment extends TitledFragment {
	private Configuration serverConfiguration;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if(serverConfiguration == null)  {
			AbstractActivity activity = (AbstractActivity)getActivity();
			serverConfiguration = activity.readObjectFromPreferences(Configuration.class,MonitorActivity.MONITOR_PREFERENCES);
			if(serverConfiguration.getUrl() == null)
				serverConfiguration.setUrl("http://192.168.0.12:8080/MonitorServer/service");		
		}
		Generator generator = new Generator(getActivity());
		return generator.createForm(serverConfiguration).getLayout();
	}
	public Configuration getServerConfiguration() {
		return serverConfiguration;
	}
	
}
