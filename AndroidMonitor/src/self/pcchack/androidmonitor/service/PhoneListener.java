package self.pcchack.androidmonitor.service;

import pl.pcchack.db.AndroidDao;
import self.pcchack.androidmonitor.model.Configuration;
import self.pcchack.androidmonitor.model.Log;
import self.pcchack.androidmonitor.model.Log.EventType;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

public class PhoneListener extends PhoneStateListener {
	private AndroidDao dao;
	private boolean callReceived,pendingCall;
	private ServerConnector connector;
	private Configuration configuration;
	public PhoneListener(AndroidDao dao,Configuration configuration,ServerConnector connector) {
		this.dao = dao;
		this.configuration = configuration;
		this.connector = connector;
	}
	@Override
	public void onCallStateChanged(int state, String incomingNumber) {
		super.onCallStateChanged(state, incomingNumber);
		if(incomingNumber == null || incomingNumber.isEmpty())
			return;
		switch(state) {
		case TelephonyManager.CALL_STATE_RINGING:
			pendingCall=true;
			handleEvent(EventType.PENDING_CALL,incomingNumber);
			break;
		case TelephonyManager.CALL_STATE_OFFHOOK:
			callReceived = true;
			handleEvent(EventType.CALL_RECEIVED,incomingNumber);
			break;
		case TelephonyManager.CALL_STATE_IDLE:
			if(pendingCall && callReceived) {
				handleEvent(EventType.CALL_FINISHED,incomingNumber);
				callReceived=false;
			} else if(pendingCall && !callReceived)
				handleEvent(EventType.HANG_UP,incomingNumber);
			pendingCall=false;
			break;
		}
	}
	private void handleEvent(EventType eventType,String incomingNumber) {
		Log log = new Log(eventType,"",incomingNumber);
		if(configuration.isSaveInDb())
			dao.save(log);
		if(configuration.isSentToServer())
			connector.sendLog(log);
	}
	
}
