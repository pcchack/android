package self.pcchack.androidmonitor.service;

import java.net.URI;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import self.pcchack.androidmonitor.model.Log;
import android.content.Context;
import android.os.StrictMode;
import android.widget.Toast;

public class ServerConnector {
	private static ServerConnector INSTANCE;
	private HttpClient client;
	private HttpPost request;
	private Context context;
	private ServerConnector(Context context) {
		client = new DefaultHttpClient();
		request = new HttpPost("http://192.168.0.12:8088/MonitorServer/service");
		this.context = context;
	}
	public void setUrl(String url) {
		request.setURI(URI.create(url));
	}
	public static ServerConnector getInstance(Context context) {
		if (INSTANCE == null) {
			INSTANCE = new ServerConnector(context);
		}
		return INSTANCE;
	}
	public static ServerConnector getInstance() {
		return getInstance(null);
	}
	public void sendLog(Log log) {
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
					.permitAll().build();
			StrictMode.setThreadPolicy(policy);
		}
		try {
			request.setEntity(new StringEntity(convertToJSON(log)));
			client.execute(request);
		} catch (Exception e) {
			android.util.Log.e("e","",e);
			if(context != null)
				Toast.makeText(context, e.getMessage(),Toast.LENGTH_SHORT).show();
		}
	}
	private String convertToJSON(Log log) throws JSONException {
		JSONObject object = new JSONObject();
		object.put("event", log.getEvent());
		object.put("phone", log.getPhone());
		object.put("occurenceTime", log.getOccurenceTime());
		object.put("content", log.getContent());
		return object.toString();
	}
}
