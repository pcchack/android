package self.pcchack.androidmonitor.model;

import java.io.Serializable;

import android.view.inputmethod.EditorInfo;
import pl.pcchack.layoutgenerator.FormField;

public class ServerKillerData implements Serializable {
	public static final long serialVersionUID=1;
	private static final int URL_ID=1,THREADS_ID=2;
	@FormField(id=URL_ID,inputType=EditorInfo.TYPE_TEXT_VARIATION_URI)
	private String url;
	@FormField(id=THREADS_ID,inputType=EditorInfo.TYPE_CLASS_NUMBER)
	private Short threads;
	
	public ServerKillerData() {
		url = "http://192.168.0.12:8088";
		threads =10;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Short getThreads() {
		return threads;
	}
	public void setThreads(Short threads) {
		this.threads = threads;
	}
	
}
