package self.pcchack.androidmonitor.model;

import java.util.Date;

import pl.pcchack.activity.ContextHolder;
import pl.pcchack.activity.IDescriptable;
import pl.pcchack.db.IEntity;
import self.model.datatype.IEnum;
import self.pcchack.androidmonitor.activity.R;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable(tableName="log")
public class Log implements IEntity,IDescriptable {
	private static final long serialVersionUID = 1L;
	@DatabaseField(columnName="id",generatedId=true)
	private Long id;
	@DatabaseField(columnName="event")
	private Byte event;
	@DatabaseField(columnName="occurence_time")
	private Date occurenceTime;
	@DatabaseField(columnName="content")
	private String content;
	@DatabaseField(columnName="phone")
	private String phone;
	public static String P_OCCURENCE_TIME="occurence_time";
	public Log() {
		
	}
	
	
	public Log(EventType event, String content, String phone) {
		super();
		setEventAsEnum(event);
		this.content = content;
		this.phone = phone;
		this.occurenceTime=new Date();
	}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Byte getEvent() {
		return event;
	}
	public void setEvent(Byte event) {
		this.event = event;
	}
	public Date getOccurenceTime() {
		return occurenceTime;
	}
	public void setOccurenceTime(Date occurenceTime) {
		this.occurenceTime = occurenceTime;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public void setEventAsEnum(EventType eventType) {
		setEvent(eventType.getValue());
	}
	public EventType getEventAsEnum() {
		return EventType.getEvent(getEvent());
	}

		
	
	public enum EventType implements IEnum<Byte> {
		SMS_RECEIVED((byte)0,getText(R.string.sms_received))
		
		,SMS_SENT((byte)1,getText(R.string.sms_sent))
		
		
		,MMS_RECEIVED((byte)2,getText(R.string.mms_received))
		
		,MMS_SEND((byte)3,getText(R.string.mms_sent))
		,PENDING_CALL((byte)4,getText(R.string.pending_call))
		,CALL_RECEIVED((byte)5,getText(R.string.call_received)),
		CALL_FINISHED((byte)6,getText(R.string.call_finished))
		,CALL_MADE((byte)7,getText(R.string.made_call))
		,HANG_UP((byte)8,getText(R.string.hang_up))
		;
		private byte value;
		private String stringValue;
		private EventType(byte value,String stringValue) {
			this.value = value;
			this.stringValue = stringValue;
		}
		
		@Override
		public String toString() {
			return stringValue;
		}

		@Override
		public Byte getValue() {
			return value;
		}
		public static String getText(int id) {
			return ContextHolder.getContext().getResources().getString(id);
		}
		public static EventType getEvent(byte value) {
			for(EventType eventType : values()) {
				if(value == eventType.getValue())
					return eventType;
			}
			return null;
		}
	}



	@Override
	public String getName() {
		return getEventAsEnum().toString();
	}


	@Override
	public String getDescription() {
		return getOccurenceTime()+" "+getPhone()+" "+getContent();
	}


	@Override
	public Integer getSrc() {
		switch(getEventAsEnum()) {
		case PENDING_CALL:
			return android.R.drawable.sym_call_incoming;
		case HANG_UP:
			return android.R.drawable.sym_call_missed;
		case CALL_MADE:
			return android.R.drawable.sym_call_outgoing;
		case SMS_SENT:
		case MMS_RECEIVED:
		case MMS_SEND:
		case SMS_RECEIVED:
			return android.R.drawable.sym_action_chat;
		case CALL_RECEIVED:
		case CALL_FINISHED:
			return android.R.drawable.sym_action_call;
		default:
			return android.R.drawable.ic_dialog_info;
			
		}
		
	}
	
}
