package self.pcchack.androidmonitor.model;

import java.io.Serializable;

import pl.pcchack.db.IEntity;
import pl.pcchack.layoutgenerator.FormField;
import self.pcchack.androidmonitor.activity.R;
import android.view.inputmethod.EditorInfo;

/**
 * @author pawel
 *
 */
public class Configuration implements Serializable,IEntity {
	private static final long serialVersionUID = 1L;
	private static final int URL_ID=1,SEND_TO_SERVER=2,SAVE_TO_DB=3;
	@FormField(id=URL_ID,inputType=EditorInfo.TYPE_TEXT_VARIATION_URI,captionId=R.string.address)
	private String url;
	@FormField(id=SEND_TO_SERVER,captionId=R.string.send_to_server)
	private boolean sentToServer;
	@FormField(id=SAVE_TO_DB,captionId=R.string.save_to_db)
	private boolean saveInDb;
	public Configuration() {
	}
	
	public Configuration(String url, boolean sentToServer, boolean saveInDb) {
		super();
		this.url = url;
		this.sentToServer = sentToServer;
		this.saveInDb = saveInDb;
	}

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public boolean isSentToServer() {
		return sentToServer;
	}
	public void setSentToServer(boolean sentToServer) {
		this.sentToServer = sentToServer;
	}
	public boolean isSaveInDb() {
		return saveInDb;
	}
	public void setSaveInDb(boolean saveInDb) {
		this.saveInDb = saveInDb;
	}
	
	
	
}
