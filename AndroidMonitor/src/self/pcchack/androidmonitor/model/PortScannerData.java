package self.pcchack.androidmonitor.model;

import java.io.Serializable;

import pl.pcchack.layoutgenerator.FormField;
import self.pcchack.androidmonitor.activity.R;
import android.view.inputmethod.EditorInfo;

public class PortScannerData implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final int URL_ID=1,PORT_START=2,PORT_FINISH=3;
	@FormField(id=URL_ID,captionId=R.string.address,inputType=EditorInfo.TYPE_TEXT_VARIATION_URI)
	private String url;
	@FormField(id=PORT_START,captionId=R.string.start_port,inputType=EditorInfo.TYPE_CLASS_NUMBER)
	private Short startPort;
	@FormField(id=PORT_FINISH,captionId=R.string.end_port,inputType=EditorInfo.TYPE_CLASS_NUMBER)
	private Short endPort;
	
	public PortScannerData() {
		url="";
		startPort=80;
		endPort=80;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Short getStartPort() {
		return startPort;
	}
	public void setStartPort(Short startPort) {
		this.startPort = startPort;
	}
	public Short getEndPort() {
		return endPort;
	}
	public void setEndPort(Short endPort) {
		this.endPort = endPort;
	}

}
