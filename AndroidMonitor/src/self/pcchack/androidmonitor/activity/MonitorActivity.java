package self.pcchack.androidmonitor.activity;

import java.util.ArrayList;
import java.util.List;

import pl.pcchack.activity.AbstractPagedActivity;
import pl.pcchack.fragment.TitledFragment;
import self.pcchack.androidmonitor.fragment.ConfigFragment;
import self.pcchack.androidmonitor.fragment.LogFragment;
import self.pcchack.androidmonitor.fragment.PortScannerFragment;
import self.pcchack.androidmonitor.fragment.ServerKillerFragment;
import android.os.Bundle;
import android.view.Window;

public class MonitorActivity extends AbstractPagedActivity {
	public static String SERVER_CONFIGURATION="configuration",LOGS_DB = "logs.db",MONITOR_PREFERENCES="monitor_preferences";
	private ConfigFragment configFragment;
	@Override
	protected int getViewPagerId() {
		return R.id.main_layout_pager;
	}

	@Override
	protected List<? extends TitledFragment> getTabs() {
		List<TitledFragment> tabs = new ArrayList<TitledFragment>();
		configFragment = new ConfigFragment();
		configFragment.setTitle(getResources().getString(R.string.config_fragment_title));
		tabs.add(configFragment);
		tabs.add(new LogFragment().setTitle(getResources().getString(R.string.log_fragment_title)));
		tabs.add(new PortScannerFragment().setTitle(getResources().getString(R.string.port_scanner)));
		tabs.add(new ServerKillerFragment().setTitle(getResources().getString(R.string.server_killer)));
		return tabs;
	}

	@Override
	protected int getMainViewId() {
		return R.layout.main_layout;
	}

	@Override
	protected void saveDataInPreferences() {
		saveObjectInPreferences(configFragment.getServerConfiguration(),MONITOR_PREFERENCES);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_PROGRESS);
		super.onCreate(savedInstanceState);
	}

	
	
	
}
