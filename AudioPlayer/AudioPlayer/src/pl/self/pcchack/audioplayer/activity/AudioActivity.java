package pl.self.pcchack.audioplayer.activity;

import java.util.ArrayList;
import java.util.List;

import pl.pcchack.activity.AbstractActivity;
import pl.self.pcchack.android.R;
import pl.self.pcchack.audio.broadcast.StartupBroadcastReceiver;
import pl.self.pcchack.audio.model.SongItem;
import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;

public class AudioActivity extends AbstractActivity implements
		android.widget.AdapterView.OnItemClickListener , OnCompletionListener {
	private static final int HEADSET_PLUGGED = 1,HEADSET_UNPLUGGED=0;
	private static final String LAST_PLAYED_SONG_INDEX="lastPlayedSongIndex",
			VOLUME="volume";
	private ListView songsView;
	private List<SongItem> songs;
	private MediaPlayer mediaPlayer;
	private SongItem actualSong;
	private ImageButton playPauseButton;
	private BroadcastReceiver receiver;
	private AudioManager manager;
	

	@Override
	protected void initFieldsAndServices(Bundle savedState) {
		readSongTitles();
		createListView();
		receiver = new HeadsetBroadcastReceiver();
		manager = (AudioManager) getSystemService(AUDIO_SERVICE);
		registerReceiver(receiver, new IntentFilter(Intent.ACTION_HEADSET_PLUG));
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		initVariables();
		if(getIntent().getBooleanExtra(StartupBroadcastReceiver.FROM_STARTUP,false))
			moveTaskToBack(true);
	}
	@Override
	protected int getMainViewId() {
		return R.layout.audio_layout;
	}
	@Override
	protected void saveDataInPreferences() {
		Editor editor =
				preferences.edit();
		editor.putInt(LAST_PLAYED_SONG_INDEX, songs.indexOf(actualSong));
		editor.putInt(VOLUME, manager.getStreamVolume(AudioManager.STREAM_MUSIC));
		editor.commit();
		
	}
	@Override
	protected boolean isKeepScreenOn() {
		return false;
	}
	
	
	private void initVariables() {
		playPauseButton =(ImageButton) findViewById(R.id.play_pause_button);
		actualSong = songs.get(preferences.getInt(LAST_PLAYED_SONG_INDEX, 0));
		manager.setStreamVolume(AudioManager.STREAM_MUSIC,
				preferences.getInt(VOLUME,5), 0);
		
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(receiver);
		cleanUp();
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		
	}

	private void readSongTitles() {
		songs = new ArrayList<SongItem>();
		Cursor cursor = getContentResolver().query(
				MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null,
				MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
		if (cursor != null && cursor.moveToFirst()) {
			int titleColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
			int artistColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
			int idColumn = cursor.getColumnIndex(MediaStore.Audio.Media._ID);
			do {
				songs.add(new SongItem(cursor.getLong(idColumn), cursor
						.getString(titleColumn),cursor.getString(artistColumn)));
			} while (cursor.moveToNext());
		}
	}
	private void createListView() {
		songsView = (ListView) findViewById(R.id.songsView);
		songsView.setAdapter(new ArrayAdapter<SongItem>(this,
				android.R.layout.simple_list_item_1, songs
						.toArray(new SongItem[songs.size()])));
		songsView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		SongItem songItem = songs.get(position);
		if (songItem.equals(actualSong))
			return;
		try {
			playSong(songItem.getId());
			actualSong = songItem;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void handleOperation(View view) {
		switch (view.getId()) {
		case R.id.play_pause_button:
			handlePlayPause();
			break;
		case R.id.stop_button:
			handleStop();
			break;
		case R.id.next_button:
			handlePlayNext();
			break;
		case R.id.previous_button:
			handlePlayPrevios();
			break;
		case R.id.actual_song_info_button:
			displayActualSongInfo();
			break;
		}
	}
	private void displayActualSongInfo() {
		showToast(actualSong.toString());
	}
	private void handleStop() {
		if (mediaPlayer != null && mediaPlayer.isPlaying()) {
			cleanUp();
			playPauseButton.setImageResource(android.R.drawable.ic_media_play);
		}
	}
	private void handlePlayPause() {
		if(mediaPlayer != null && mediaPlayer.isPlaying()) {
			mediaPlayer.pause();
			playPauseButton.setImageResource(android.R.drawable.ic_media_play);
		} else {
			if(actualSong != null) {
				if(mediaPlayer != null)
					mediaPlayer.start();
				else
					playSongOnIndex(songs.indexOf(actualSong));
			}
			else 
				playSongOnIndex(0);
			playPauseButton.setImageResource(android.R.drawable.ic_media_pause);
		}
	}
	private void handlePlayNext() {
		if (actualSong != null) {
			playSongOnIndex(songs.indexOf(actualSong)+1);
		}
	}
	private void handlePlayPrevios() {
		if(actualSong != null) {
			playSongOnIndex(songs.indexOf(actualSong)-1);
		}
	}
	private void playSongOnIndex(int index) {
		if(index >= songs.size())
			index = 0;
		if(index < 0)
			index = songs.size()-1;
		actualSong = songs.get(index);
		playSong(actualSong.getId());
	}
	private void playSong(Long songId) {
		try {
			cleanUp();
			mediaPlayer = new MediaPlayer();
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			mediaPlayer.setDataSource(this, ContentUris.withAppendedId(
					MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, songId));
			mediaPlayer.prepare();
			mediaPlayer.start();
			mediaPlayer.setOnCompletionListener(this);
			playPauseButton.setImageResource(android.R.drawable.ic_media_pause);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void cleanUp() {
		if(mediaPlayer != null) {
			if(mediaPlayer.isPlaying())
				mediaPlayer.stop();
			playPauseButton.setImageResource(android.R.drawable.ic_media_play);
			mediaPlayer.release();
			mediaPlayer = null;
		}
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		handlePlayNext();
	}
	public class HeadsetBroadcastReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
				int state = intent.getIntExtra("state",-1);
				switch(state) {
				case HEADSET_PLUGGED:
					int lastPlayedSongIndex =
					preferences.getInt(LAST_PLAYED_SONG_INDEX, 0);
					playSongOnIndex(lastPlayedSongIndex);
					break;
				case HEADSET_UNPLUGGED:
					cleanUp();
					moveTaskToBack(true);
					break;
				}
			}
			
		}
		
	}

	
	

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_audio,
					container, false);
			return rootView;
		}
	}



}
