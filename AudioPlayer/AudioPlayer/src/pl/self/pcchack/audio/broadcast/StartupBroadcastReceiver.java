package pl.self.pcchack.audio.broadcast;

import pl.self.pcchack.audioplayer.activity.AudioActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;


public class StartupBroadcastReceiver extends BroadcastReceiver {
	public static final String FROM_STARTUP="fromStartup";
	@Override
	public void onReceive(Context context, Intent intent) {
		Toast.makeText(context,"Activity has been started",Toast.LENGTH_SHORT).show();
		Intent audioActivityStart  = new Intent(context,AudioActivity.class);
		audioActivityStart.putExtra(FROM_STARTUP,true);
		audioActivityStart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(audioActivityStart);
		
	}
	
}

