package pl.pcchack.android.voter;

import java.util.List;

import pl.pcchack.activity.ListViewAdapter;
import pl.pcchack.android.voter.model.User;
import pl.pcchack.db.AndroidDao;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class UserFragment extends Fragment {
	private ListView userListView;
	private List<User> users;
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View mainView = inflater.inflate(R.layout.voter_users_fragment, container,false);
		initComponents(mainView);
		
		return mainView;
	}
	private void initComponents(View mainView) {
		users = AndroidDao.getLastInstance().getAll(User.class);
		userListView =(ListView) mainView.findViewById(R.id.user_list_view);
		userListView.setAdapter(new ListViewAdapter<User>(getActivity(), users));
	}
	public void handleOperation(View view) {
		switch(view.getId()) {
		case R.id.g_add_button:
			break;
		case R.id.g_edit_button:
			break;
		case R.id.g_delete_button:
			break;
		}
	}
	
}
