package pl.pcchack.android.voter;

import java.util.Arrays;
import java.util.List;

import pl.pcchack.activity.AbstractActivity;
import pl.pcchack.android.voter.model.PoliticalParty;
import pl.pcchack.android.voter.model.Politican;
import pl.pcchack.android.voter.model.Sounding;
import pl.pcchack.android.voter.model.User;
import pl.pcchack.android.voter.model.User.RoleEnum;
import pl.pcchack.android.voter.model.Vote;
import pl.pcchack.db.AndroidDao;
import pl.pcchack.db.AndroidDao.TransactionInfo;
import pl.pcchack.db.IEntity;
import self.utility.Hasher;
import self.utility.ObjectUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class VoterLoginActivity extends AbstractActivity {
	private TextView loginTitleLabel;
	private EditText loginEdit,passwordEdit;
	private AndroidDao androidDao;
	public static final String DB_NAME="voter_db.db",LOGGED_USER="loggedUser",DAO="dao";
	@SuppressWarnings("unchecked")
	@Override
	protected void initFieldsAndServices(Bundle savedState) {
		loginTitleLabel = getTextView(R.id.g_login_title);
		loginTitleLabel.setText(R.string.login_title);
		loginEdit = getEditText(R.id.g_login_edit);
		passwordEdit = getEditText(R.id.g_password_edit);
		
		androidDao = new AndroidDao(this,
				DB_NAME,(List<Class<? extends IEntity>>) (Object) 
				Arrays.asList(User.class,Vote.class,
						Sounding.class,
						Politican.class,
						PoliticalParty.class));
		
	}

	@Override
	protected int getMainViewId() {
		return R.layout.g_login_layout;
	}

	@Override
	protected void saveDataInPreferences() {

	}

	@Override
	protected boolean isKeepScreenOn() {
		return true;
	}
	public void handleOperation(View view) {
		switch(view.getId()) {
		case R.id.g_login_button:
			User user  = tryToLogin();
			if(user == null)
				return;
			Intent intent = new Intent(this,
					user.getRoleAsEnum() == RoleEnum.USER ?
					VoterActivity.class : VoterAdminActivity.class);
			intent.putExtra(LOGGED_USER,user);
			startActivity(intent);
			finish();
			break;
		}
	}
	private User tryToLogin() {
		List<User> users = androidDao.getAll(User.class);
		if(ObjectUtil.nullOrEmpty(users)) {
			return handleNoUsersInDb();
		}
		User user = androidDao.getRealDao(User.class).queryForId(loginEdit.getText().toString());
		if(user != null && user.getPassword().equals(Hasher.createMD5(passwordEdit.getText().toString())))
			return user;
		return null;
	}
	private User handleNoUsersInDb() {
		final User adminUser = new User();
		adminUser.setLogin("admin");
		adminUser.setPassword(Hasher.createMD5("admin"));
		adminUser.setRoleAsEnum(RoleEnum.ADMIN);
		TransactionInfo info = androidDao.runInTransaction(new Runnable() {
			@Override
			public void run() {
				androidDao.save(adminUser);
			}
		});
		if(info.isSuccess())
			return adminUser;
		return null;
	}
}
