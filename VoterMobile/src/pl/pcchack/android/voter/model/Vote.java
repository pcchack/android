package pl.pcchack.android.voter.model;

import pl.pcchack.db.IEntity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable(tableName="vote")
public class Vote  implements IEntity{
	private static final long serialVersionUID = 1L;
	@DatabaseField(columnName="vote_id",generatedId=true)
	private Long voteId;
	@DatabaseField(columnName="politican_id",foreign=true)
	private Politican politican;
	@DatabaseField(columnName="sounding_id",foreign=true)
	private Sounding sounding;
	public Long getVoteId() {
		return voteId;
	}
	public void setVoteId(Long voteId) {
		this.voteId = voteId;
	}
	public Politican getPolitican() {
		return politican;
	}
	public void setPolitican(Politican politican) {
		this.politican = politican;
	}
	public Sounding getSounding() {
		return sounding;
	}
	public void setSounding(Sounding sounding) {
		this.sounding = sounding;
	}
	
}
