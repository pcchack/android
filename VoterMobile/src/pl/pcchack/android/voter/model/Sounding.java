package pl.pcchack.android.voter.model;

import java.util.Collection;
import java.util.Date;

import pl.pcchack.db.IEntity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable(tableName="sounding")
public class Sounding implements IEntity {
	private static final long serialVersionUID = 1L;
	@DatabaseField(columnName="sounding_id",generatedId=true)
	private Long soundingId;
	@DatabaseField(columnName="start_date")
	private Date startDate;
	@DatabaseField(columnName="end_date")
	private Date endDate;
	@ForeignCollectionField
	private Collection<Vote> votes;
	public Long getSoundingId() {
		return soundingId;
	}
	public void setSoundingId(Long soundingId) {
		this.soundingId = soundingId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Collection<Vote> getVotes() {
		return votes;
	}
	public void setVotes(Collection<Vote> votes) {
		this.votes = votes;
	}
	
}
