package pl.pcchack.android.voter.model;

import pl.pcchack.db.IEntity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable(tableName="politican")
public class Politican implements IEntity {
	private static final long serialVersionUID = 1L;
	@DatabaseField(columnName="politican_id",generatedId=true)
	private Long politicanId;
	@DatabaseField(columnName="name")
	private String name;
	@DatabaseField(columnName="surname")
	private String surname;
	@DatabaseField(columnName="age")
	private Short age;
	@DatabaseField(columnName="political_party_id",foreign=true)
	private PoliticalParty politicalParty;
	public Long getPoliticanId() {
		return politicanId;
	}
	public void setPoliticanId(Long politicanId) {
		this.politicanId = politicanId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public Short getAge() {
		return age;
	}
	public void setAge(Short age) {
		this.age = age;
	}
	public PoliticalParty getPoliticalParty() {
		return politicalParty;
	}
	public void setPoliticalParty(PoliticalParty politicalParty) {
		this.politicalParty = politicalParty;
	}
	
}
