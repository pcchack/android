package pl.pcchack.android.voter.model;

import pl.pcchack.activity.IDescriptable;
import pl.pcchack.db.IEntity;
import self.model.datatype.IEnum;
import self.utility.EnumResolver;
import android.R;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
@DatabaseTable(tableName="user")
public class User implements IEntity,IDescriptable {
	private static final long serialVersionUID = 1L;
	
	
	
	@DatabaseField(columnName="login",id=true)
	private String login;
	@DatabaseField(columnName="password")
	private String password;
	@DatabaseField(columnName="role")
	private Byte role;
	public User() {

	}
	public User(String login, String password) {
		super();
		this.login = login;
		this.password = password;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Byte getRole() {
		return role;
	}
	public void setRole(Byte role) {
		this.role = role;
	}
	public RoleEnum getRoleAsEnum() {
		return EnumResolver.getEnum(RoleEnum.values(),getRole());
	}
	public void setRoleAsEnum(RoleEnum roleEnum) {
		setRole(roleEnum.getValue());
	}
	public enum RoleEnum implements IEnum<Byte> {
		ADMIN((byte)0),USER((byte)1);
		
		private RoleEnum(byte value) {
			this.value = value;
		}
		private byte value;
		@Override
		public Byte getValue() {
			return value;
		}
		
	}
	@Override
	public String getName() {
		return getLogin();
	}
	@Override
	public String getDescription() {
		return "";
	}
	@Override
	public Integer getSrc() {
		return R.drawable.sym_contact_card;
	}
	
}
