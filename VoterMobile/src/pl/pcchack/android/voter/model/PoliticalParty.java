package pl.pcchack.android.voter.model;

import java.util.Collection;

import pl.pcchack.db.IEntity;
import self.model.datatype.IEnum;
import self.utility.EnumResolver;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName="political_party")
public class PoliticalParty implements IEntity {

	private static final long serialVersionUID = 1L;
	@DatabaseField(columnName="political_party_id",generatedId=true)
	private Long politicalPartyId;
	@DatabaseField(columnName="name")
	private String name;
	@DatabaseField(columnName="short_name")
	private String shortName;
	@DatabaseField(columnName="political_direction")
	private Byte politicalDirection;
	@ForeignCollectionField
	private Collection<Politican> politicans;
	
	public Long getPoliticalPartyId() {
		return politicalPartyId;
	}
	public void setPoliticalPartyId(Long politicalPartyId) {
		this.politicalPartyId = politicalPartyId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public Byte getPoliticalDirection() {
		return politicalDirection;
	}
	public PoliticalDirection getPoliticalDirectionAsEnum() {
		return EnumResolver.getEnum(PoliticalDirection.values(),
				getPoliticalDirection());
	}
	public void setPoliticalDirection(Byte politicalDirection) {
		this.politicalDirection = politicalDirection;
	}
	public void setPoliticalDirectionAsEnum(PoliticalDirection value) {
		setPoliticalDirection(value.getValue());
	}
	public Collection<Politican> getPoliticans() {
		return politicans;
	}
	public void setPoliticans(Collection<Politican> politicans) {
		this.politicans = politicans;
	}


	public enum PoliticalDirection implements IEnum<Byte>{
		LEFT((byte)0),CENTER_LEFT((byte)1),CENTER((byte)2),
		CENTER_RIGTH((byte)3),RIGHT((byte)4);
		private byte value;
		
		private PoliticalDirection(byte value) {
			this.value = value;
		}

		@Override
		public Byte getValue() {
			return value;
		}
		
	}
	
}
