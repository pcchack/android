package pl.pcchack.android.voter;

import pl.pcchack.activity.AbstractActivity;
import pl.pcchack.activity.ListViewFragment;
import pl.pcchack.android.voter.model.User;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class VoterAdminActivity extends AbstractActivity implements
		ActionBar.TabListener, ViewPager.OnPageChangeListener {
	private ViewPager viewPager;
	private TabsPagerAdapter adapter;
	private ActionBar actionBar;
	private User loggedInUser;
	private String[] tabs = { "users", "parties", "soundings", "politicans" };

	@Override
	protected void initFieldsAndServices(Bundle savedState) {
		createViewPager();
		createActionBar();
		loggedInUser = (User) getIntent().getSerializableExtra(
				VoterLoginActivity.LOGGED_USER);

	}

	private void createViewPager() {
		viewPager = (ViewPager) findViewById(R.id.pager);
		adapter = new TabsPagerAdapter(getSupportFragmentManager());
		viewPager.setAdapter(adapter);
		viewPager.setOnPageChangeListener(this);
	}

	private void createActionBar() {
		actionBar = getActionBar();
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		for (String tab : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab)
					.setTabListener(this));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.voter_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected int getMainViewId() {
		return R.layout.voter_admin_layout;
	}

	@Override
	protected void saveDataInPreferences() {
		// TODO Auto-generated method stub

	}

	@Override
	protected boolean isKeepScreenOn() {
		// TODO Auto-generated method stub
		return false;
	}

	public class TabsPagerAdapter extends FragmentPagerAdapter {

		public TabsPagerAdapter(FragmentManager fm) {
			super(fm);

		}
		@Override
		public ListViewFragment<?> getItem(int index) {
			ListViewFragment<User> fragment =   new ListViewFragment<User>();
			Bundle bundle = new Bundle();
			bundle.putInt(ListViewFragment.FRAGMENT_ID,R.layout.voter_users_fragment);
			bundle.putInt(ListViewFragment.LIST_VIEW_ID,R.id.user_list_view);
			bundle.putSerializable(ListViewFragment.ITEM_TYPE,User.class);
			fragment.setArguments(bundle);
			return fragment;
//			switch (index) {
//			case 0:
//				
////			case 1:
////				return new SoundingFragment();
////			case 2:
////				return new PoliticalPartyFragment();
////			case 3:
////				return new PoliticanFragment();
//			}
//			return null;
		}

		@Override
		public int getCount() {

			return 4;
		}

	}

	public static class SoundingFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			return inflater.inflate(R.layout.voter_sounding_fragment,
					container, false);
		}
	}

	public static class PoliticalPartyFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			return inflater.inflate(R.layout.voter_political_parties_fragment,
					container, false);
		}
	}

	public static class PoliticanFragment extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			return inflater.inflate(R.layout.voter_politicans_fragment,
					container, false);
		}
	}
	public void handleOperation(View view) {
		adapter.getItem(viewPager.getCurrentItem()).handleOperation(view);
	}
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		viewPager.setCurrentItem(tab.getPosition());
		
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageSelected(int position) {
		actionBar.setSelectedNavigationItem(position);

	}
}
