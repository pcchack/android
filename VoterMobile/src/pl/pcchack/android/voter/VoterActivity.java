package pl.pcchack.android.voter;


import pl.pcchack.activity.AbstractActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
public class VoterActivity extends AbstractActivity {



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.voter_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
	@Override
	protected void initFieldsAndServices(Bundle savedState) {
		
	}

	@Override
	protected int getMainViewId() {
		return R.layout.voter_layout;
	}

	@Override
	protected void saveDataInPreferences() {
		
	}

	@Override
	protected boolean isKeepScreenOn() {
		return true;
	}


}
