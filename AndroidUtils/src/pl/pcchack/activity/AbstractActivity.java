package pl.pcchack.activity;

import pl.pcchack.android.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.FragmentActivity;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public abstract class AbstractActivity extends FragmentActivity{
	protected SharedPreferences preferences;
	protected PowerManager.WakeLock wakeLock;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(getMainViewId());
		preferences = getPreferences(Context.MODE_PRIVATE);
		initFieldsAndServices();
		if(isKeepScreenOn())
			keepScreenOn();
		
	}
	protected abstract void initFieldsAndServices();
	protected abstract int getMainViewId();
	protected abstract void saveDataInPreferences();
	protected abstract boolean isKeepScreenOn();
	
	public void showConfirm(String message,DialogInterface.OnClickListener acceptListener) {
		new AlertDialog.Builder(this,AlertDialog.THEME_HOLO_DARK)
		.setIcon(android.R.drawable.ic_dialog_info)
		.setPositiveButton(R.string.g_accept,acceptListener)
		.setNegativeButton(R.string.g_cancel,new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				
			}
		}).setMessage(message).create().show();
	}
	
	public TextView getTextView(int id) {
		return (TextView) findViewById(id);
	}
	public EditText getEditText(int id) {
		return (EditText) findViewById(id);
	}
	public Button getButton(int id) {
		return (Button) findViewById(id);
	}
	public Switch getSwitch(int id) {
		return (Switch) findViewById(id);
	}
	public CheckBox getCheckBox(int id) {
		return (CheckBox) findViewById(id);
	}
	public RadioButton getRadioButton(int id) {
		return (RadioButton) findViewById(id);
	}
	
	
	protected void keepScreenOn() {
		wakeLock = ((PowerManager)getSystemService(Context.POWER_SERVICE)).newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK,
				"");
		wakeLock.acquire();
	}
	
	public SharedPreferences getPreferences() {
		return preferences;
	}
	@Override
	protected void onPause() {
		super.onPause();
		if(isKeepScreenOn())
			wakeLock.release();
	}

	@Override
	protected void onStop() {
		super.onStop();
		saveDataInPreferences();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if(isKeepScreenOn())
			wakeLock.acquire();
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();

	}
	public void showToast(String message) {
		showToast(message, Toast.LENGTH_SHORT);
	}
	public void showToast(String message,int lenght) {
		Toast.makeText(this,message,lenght).show();
	}

}
