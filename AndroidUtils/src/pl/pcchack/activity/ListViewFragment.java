package pl.pcchack.activity;

import java.util.List;

import pl.pcchack.android.R;
import pl.pcchack.db.AndroidDao;
import pl.pcchack.db.IEntity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class ListViewFragment<T extends IDescriptable & IEntity> extends Fragment implements IOperationHandler{
	private List<T> items;
	private ListView itemsListView;

	public static String FRAGMENT_ID="fragmentId",LIST_VIEW_ID="listViewId",ITEM_TYPE="itemType";
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		initVariables();
		View mainView = inflater.inflate(getArguments().getInt(FRAGMENT_ID), container,false);
		initComponents(mainView);
		return mainView;
	}
	private void initComponents(View mainView) {

		itemsListView =(ListView) mainView.findViewById(getArguments().getInt(LIST_VIEW_ID));
		itemsListView.setAdapter(new ListViewAdapter<T>(getActivity(), items));
	}
	private void initVariables() {
		items = AndroidDao.getLastInstance().getAll(getType());
	}
	public void handleOperation(View view) {
		if(view.getId() ==  R.id.g_add_button) {
			
		} else if(view.getId() ==  R.id.g_edit_button) {
			
		} else if(view.getId() == R.id.g_delete_button) {
		
		}
	}
	public Class<T> getType() {
		return (Class<T>)getArguments().getSerializable(ITEM_TYPE);
	}
}
