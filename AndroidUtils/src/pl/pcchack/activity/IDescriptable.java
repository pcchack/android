package pl.pcchack.activity;

public interface IDescriptable {
	String getName();
	String getDescription();
	Integer getSrc();
}
