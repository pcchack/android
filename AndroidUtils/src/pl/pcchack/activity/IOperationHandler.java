package pl.pcchack.activity;

import android.view.View;

public interface IOperationHandler {
	void handleOperation(View view);
}
