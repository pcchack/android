package pl.pcchack.activity;

import java.util.List;

import pl.pcchack.android.R;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

public class ListViewAdapter<T extends IDescriptable> extends ArrayAdapter<T>{
	private List<T> items;
	public ListViewAdapter(Context context,List<T> items) {
		super(context,R.layout.g_list_item,items);
		this.items = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View itemView = convertView;
		if(itemView == null) {
			itemView =((Activity)getContext()).getLayoutInflater().inflate(R.layout.g_list_item,
					null);
			itemView.setTag(createHolder(itemView));
		}
		loadData(itemView,position);
		return itemView;
	}
	private Object createHolder(View itemView) {
		Holder holder = new Holder();
		holder.image =(ImageView)
				itemView.findViewById(R.id.g_item_image);
		holder.name = (TextView) itemView.findViewById(R.id.g_item_name);
		holder.description = (TextView) itemView.findViewById(R.id.g_item_description);
		holder.check = (ToggleButton) itemView.findViewById(R.id.g_item_check);
		return holder;
	}
	private void loadData(View itemView,int position) {
		Holder holder = (Holder) itemView.getTag();
		T item = items.get(position);
		holder.image.setImageResource(item.getSrc());
		holder.name.setText(item.getName());
		holder.description.setText(item.getDescription());
	}
	static class Holder {
		private ImageView image;
		private TextView name;
		private TextView description;
		private ToggleButton check;
	}
}
