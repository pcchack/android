package pl.pcchack.db;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class AndroidDao extends OrmLiteSqliteOpenHelper implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final int DB_VERSION=1;
	private List<Class<? extends IEntity>> entityTypes;
	private Map<Class<? extends IEntity>,RuntimeExceptionDao<Class<? extends IEntity>,Object>> daoMap;
	private SQLiteDatabase db;
	private static AndroidDao INSTANCE;
	public AndroidDao(Context context, String databaseName,List<Class<? extends IEntity>> entityTypes) {
		super(context, databaseName, null, DB_VERSION);
		this.entityTypes = entityTypes;
		daoMap = new HashMap<Class<? extends IEntity>,RuntimeExceptionDao<Class<? extends IEntity>,Object>>();
		INSTANCE = this;
	}
	public static AndroidDao getLastInstance() {
		return INSTANCE;
	}
	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		try {

		for(Class<? extends IEntity> entityType : entityTypes) {
			TableUtils.createTable(connectionSource, entityType);
		}
		}  catch (java.sql.SQLException e) {
			Log.e("","Can't create database",e);
			throw new RuntimeException(e);
		}
		
	}
	
	@Override
	public void onOpen(SQLiteDatabase db) {
		super.onOpen(db);
		this.db = db;
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, ConnectionSource arg1, int arg2,
			int arg3) {
		try {
		for(Class<? extends IEntity> entityType : entityTypes) {
			TableUtils.dropTable(arg1,entityType,true);
		}
		} catch(java.sql.SQLException e) {
			Log.e("","Can't drop database",e);
			throw new RuntimeException(e);
		}
		
	}

	public  <T extends IEntity> void save(T entity) {
		getRealDao(getEntityType(entity)).create(entity);
	}
	public <T extends IEntity> void update(T entity) {
		getRealDao(getEntityType(entity)).update(entity);
	}
	public <T extends IEntity> void delete(T entity) {
		getRealDao(getEntityType(entity)).delete(entity);
	}
	public <T extends IEntity> List<T> getAll(Class<T> entityType) {
		return getRealDao(entityType).queryForAll();
	}
	public <T extends IEntity> void deleteAll(Class<T> entityType) {
		RuntimeExceptionDao<T,Object> dao = getRealDao(entityType);
		dao.delete(dao.queryForAll());
	}
	public TransactionInfo runInTransaction(Runnable code) {
		try {
			db.beginTransaction();
			code.run();
			db.setTransactionSuccessful();
			return new TransactionInfo("",true);
		} catch(Exception e) {
			Log.e("",e.getMessage(),e);
			return new TransactionInfo(e.getMessage(),false);
			
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T extends IEntity> RuntimeExceptionDao<T,Object> getRealDao(Class<T> entityType) {
		if(daoMap.containsKey(entityType))
			return (RuntimeExceptionDao<T,Object>) (Object) daoMap.get(entityType);
		RuntimeExceptionDao<T, Object> dao =  getRuntimeExceptionDao(entityType);
		
		daoMap.put((Class<? extends IEntity>)(Object)entityType,(RuntimeExceptionDao<Class<? extends IEntity>,Object>)(Object) dao);
		return dao;
	}
	@SuppressWarnings("unchecked")
	public <T extends IEntity> Class<T> getEntityType(T entity) {
		return (Class<T>) entity.getClass();
	}
	public class TransactionInfo {
		private String message;
		private boolean success;
		
		public TransactionInfo(String message, boolean success) {
			super();
			this.message = message;
			this.success = success;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public boolean isSuccess() {
			return success;
		}
		public void setSuccess(boolean success) {
			this.success = success;
		}
		
	}
}
