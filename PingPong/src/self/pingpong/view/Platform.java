package self.pingpong.view;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;

public class Platform extends AbstractDrawableElement{
	private Bitmap platformBitmap;
	@Override
	public void draw(Canvas canvas) {
		canvas.drawBitmap(bitmap,null,
			   new Rect((int)getX(), (int)getY(),(int)getXPlusLength(), (int)getY()+30),
				
				painter);
	}

	@Override
	protected void initializePainterAndOthers() {
		painter.setColor(Color.CYAN);

	}

	@Override
	public AbstractDrawableElement setLength(float length) {
		if(length<=10)
			length=10;
		return super.setLength(length);
	}
	
	
}
